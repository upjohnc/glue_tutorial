# need to run the role creation first and then add this inline policy to that role
# {
#     "Version": "2012-10-17",
#     "Statement": [
#         {
#             "Effect": "Allow",
#             "Action": [
#                 "s3:GetObject",
#                 "s3:PutObject"
#             ],
#             "Resource": [
#                 "arn:aws:s3:::nothing-chad-nothing*"
#             ]
#         }
#     ]
# }

resource "aws_s3_bucket" "glue_bucket" {
  bucket = "nothing-chad-nothing"
}

resource "aws_glue_job" "tutorial_example" {
  name     = "glue-blog-tutorial-job"
  role_arn = aws_iam_role.a_glue_role.arn

  command {
    script_location = "s3://${aws_s3_bucket.glue_bucket.bucket}/spark_script/glue_script.py"
  }

  glue_version      = "2.0"
  timeout           = "10"
  number_of_workers = "2"
  worker_type       = "Standard"

  default_arguments = {
    "--continuous-log-logGroup"          = aws_cloudwatch_log_group.glue_logs.name
    "--enable-continuous-cloudwatch-log" = "true"
    "---enable-continuous-log-filter"    = "true"
    "--enable-metrics"                   = ""
    "--extra-py-files"                   = "s3://${aws_s3_bucket.glue_bucket.bucket}/spark_script/services.zip"
  }
}

resource "aws_cloudwatch_log_group" "glue_logs" {
  name              = "glue_logs"
  retention_in_days = "1"
}

resource "aws_glue_crawler" "movie_data-11" {
  database_name = "glue-blog-tutorial-db"
  name          = "glue-blog-tutorial-crawler-111"
  role          = aws_iam_role.a_glue_role.name


  s3_target {
    path = "s3://${aws_s3_bucket.glue_bucket.bucket}/crawler_data/"
  }

  schema_change_policy {
    delete_behavior = "DEPRECATE_IN_DATABASE"
    update_behavior = "UPDATE_IN_DATABASE"
  }
}

resource "aws_iam_role" "a_glue_role" {
  name               = "a_glue_role"
  assume_role_policy = data.aws_iam_policy_document.glue_assumed_role.json
}

resource "aws_iam_role_policy_attachment" "glue_managed_policy" {
  role       = aws_iam_role.a_glue_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSGlueServiceRole"
}

data "aws_iam_policy_document" "glue_assumed_role" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["glue.amazonaws.com"]
    }
  }
}


