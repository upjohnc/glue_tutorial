from invoke import task

BUCKET_NAME = "nothing-chad-nothing"


@task
def upload_data_file(c, bucket_name=BUCKET_NAME, prefix="crawler_data"):
    c.run(f"aws s3 cp movie_data.txt s3://{bucket_name}/{prefix}/movie_data.txt")


@task
def upload_script(c, bucket_name=BUCKET_NAME, prefix="spark_script"):
    c.run("cd src && zip -r services.zip services/")
    c.run(f"aws s3 cp ./src/services.zip s3://{bucket_name}/{prefix}/services.zip")
    c.run(f"aws s3 cp ./src/glue_script.py s3://{bucket_name}/{prefix}/glue_script.py")
    c.run("rm ./src/*.zip")
    # c.run(
    #     f"aws s3 cp ./src/glue_script_utils.py s3://{bucket_name}/{prefix}/glue_script_utils.py"
    # )


@task
def upload_files(c, bucket_name=BUCKET_NAME):
    upload_data_file(c, bucket_name=bucket_name)
    upload_script(c, bucket_name=bucket_name)
