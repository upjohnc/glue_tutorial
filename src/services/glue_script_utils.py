import pyspark.sql.functions as f
from pyspark.sql import DataFrame


def add_decade(input_df: DataFrame) -> DataFrame:
    # Create a decade column from year
    decade_col = f.floor(input_df["year"] / 10) * 10
    data_frame = input_df.withColumn("decade", decade_col)
    return data_frame


def df_aggregation(input_df: DataFrame) -> DataFrame:
    # Group by decade: Count movies, get average rating
    data_frame_aggregated = input_df.groupby("decade").agg(
        f.count(f.col("movie_title")).alias("movie_count"),
        f.mean(f.col("rating")).alias("rating_mean"),
    )
    return data_frame_aggregated
