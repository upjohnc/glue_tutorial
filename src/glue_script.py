from datetime import datetime

import pyspark.sql.functions as f
from awsglue.context import GlueContext
from awsglue.dynamicframe import DynamicFrame
from pyspark.context import SparkContext
from services.glue_script_utils import add_decade, df_aggregation

# these imports were in the tutorial but I don't know why
# from awsglue.job import Job
# from awsglue.utils import getResolvedOptions

spark_context = SparkContext.getOrCreate()
glue_context = GlueContext(spark_context)
session = glue_context.spark_session

# Parameters
GLUE_DB = "glue-blog-tutorial-db"
GLUE_TABLE = "crawler_data"
S3_WRITE_PATH = "s3://nothing-chad-nothing/write"

#########################################
# EXTRACT (READ DATA)
#########################################


# Log starting time
dt_start = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
print("Start time:", dt_start)

# Read movie data to Glue dynamic frame
chad_frame = dynamic_frame_read = glue_context.create_dynamic_frame.from_catalog(
    database=GLUE_DB, table_name=GLUE_TABLE
)

# decade_col = f.floor(chad_frame["year"] / 10) * 10
# chad_frame = chad_frame.withColumn("decade", decade_col)
# chad_frame.show(20)

# Convert dynamic frame to data frame to use standard pyspark functions
data_frame = dynamic_frame_read.toDF()

#########################################
# TRANSFORM (MODIFY DATA)
#########################################

data_frame_with_decade = add_decade(data_frame)

data_frame_aggregated = df_aggregation(data_frame_with_decade)

# Sort by the number of movies per the decade
data_frame_aggregated = data_frame_aggregated.orderBy(f.desc("movie_count"))

# Print result table
# Note: Show function is an action. Actions force the execution of the data frame plan.
# With big data the slowdown would be significant without caching.
data_frame_aggregated.show(10)

#########################################
# LOAD (WRITE DATA)
#########################################

# Create just 1 partition, because there is so little data
data_frame_aggregated = data_frame_aggregated.repartition(1)

# Convert back to dynamic frame
dynamic_frame_write = DynamicFrame.fromDF(
    data_frame_aggregated, glue_context, "dynamic_frame_write"
)

# Write data back to S3
glue_context.write_dynamic_frame.from_options(
    frame=dynamic_frame_write,
    connection_type="s3",
    connection_options={
        "path": S3_WRITE_PATH,
        # Here you could create S3 prefixes according to a values in specified columns
        # "partitionKeys": ["decade"]
    },
    format="csv",
)

# Log end time
dt_end = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
print("Start time:", dt_end)
