from main import sample_transform
from pytest import assume
from services.glue_script_utils import add_decade, df_aggregation

# import glue_script


def test_sample_transform(spark_session):
    test_df = spark_session.createDataFrame(
        [
            ("hobbit", "samwise", 5),
            ("hobbit", "bilbo", 50),
            ("hobbit", "bilbo", 20),
            ("wizard", "gandalf", 1000),
        ],
        ["that_column", "another_column", "yet_another"],
    )
    new_df = sample_transform(test_df)

    with assume:
        assert new_df.toPandas().to_dict("list")["new_column"][0] == 70
    with assume:
        assert new_df.count() == 1


input_columns = [
    "movie_title",
    "year",
    "rating",
]


def test_add_decade(spark_session):
    input_data = [
        ("The Shawshank Redemption", 1994, 9.2),
        ("The Godfather", 1972, 9.2),
        ("The Godfather: Part II", 1974, 9.0),
        ("The Dark Knight", 2008, 9.0),
        ("12 Angry Men", 1957, 8.9),
        ("Schindler's List", 1993, 8.9),
        ("The Lord of the Rings: The Return of the King", 2003, 8.9),
        ("Pulp Fiction", 1994, 8.9),
        ("The Lord of the Rings: The Fellowship of the Ring", 2001, 8.8),
        ("Fight Club", 1999, 8.8),
    ]
    test_df = spark_session.createDataFrame(input_data, input_columns)
    result_df = add_decade(test_df)
    result_decade = result_df.toPandas().to_dict("list")["decade"]
    with assume:
        assert result_df.count() == len(input_data)
    with assume:
        assert result_decade[0] == 1990
    with assume:
        assert result_decade[3] == 2000


def test_df_aggregation(spark_session):
    input_data = [
        ("The Shawshank Redemption", 1994, 9.2, 1990),
        ("The Godfather", 1972, 9.2, 1970),
        ("The Godfather: Part II", 1974, 9.0, 1970),
        ("The Dark Knight", 2008, 9.0, 2000),
        ("12 Angry Men", 1957, 8.9, 1950),
        ("Schindler's List", 1993, 8.9, 1990),
        ("The Lord of the Rings: The Return of the King", 2003, 8.9, 2000),
        ("Pulp Fiction", 1994, 8.9, 1990),
        ("The Lord of the Rings: The Fellowship of the Ring", 2001, 8.8, 2000),
        ("Fight Club", 1999, 8.8, 1990),
    ]
    input_columns_with_decade = input_columns + ["decade"]
    test_df = spark_session.createDataFrame(input_data, input_columns_with_decade)
    result_df = df_aggregation(test_df)
    result_pandas = result_df.toPandas()

    with assume:
        assert result_df.count() == 4

    decade_column_1990 = result_pandas.loc[result_pandas["decade"] == 1990]
    with assume:
        assert decade_column_1990["movie_count"].iloc[0] == 4
    with assume:
        assert round(decade_column_1990["rating_mean"].iloc[0], 2) == 8.95
