# Glue Tutorial

This is the setting up of a tutorial in terraform.  The tutorial is this [blog post](https://data.solita.fi/aws-glue-tutorial-with-spark-and-python-for-data-developers/).

## Terraform setup

Running `terraform apply` will set up the glue crawler and the glue job.  You will need to add an `inline policy`
in the aws console to the role that is created.  The inline policy will provide access to the S3 bucket.
Due to some permission reason the A Cloud Guru account will not let the inline policy be set through terraform.

- `terraform apply`
- add inline policy in aws console to the `a_glue_role` role
- run `poetry run invoke upload-files`
  - this will copy the movie data and the spark script to the s3 bucket
- in the aws console, run the glue crawler
- in the aws console, run the glue job
  - in `s3://nothing-chad-nothing/write` will be the output from the aws console
  - README.md

