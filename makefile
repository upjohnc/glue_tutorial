.RECIPEPREFIX = >

build-test:
> docker build --tag spark-test ./docker

run-test:
> docker compose -f docker/docker-compose.yml run --rm spark-test

run-test-with-glue-added:
> docker compose -f docker/docker-compose.yml run --rm spark-test

run-test-glue:
> docker compose -f docker/docker-compose.yml run --rm spark-test-glue

run-test-glue-aws-image:
> docker compose -f docker/docker-compose.yml run --rm spark-test-aws-image
